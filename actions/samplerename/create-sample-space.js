define(function(require, exports, module) {

    var UI = require("ui");
    var ContentHelpers = require("content-helpers");

    return UI.registerAction("create-sample-space", UI.AbstractUIAction.extend({

        defaultConfiguration: function() {

            var config = this.base();

            config.title = "Create Sample Space Test";
            config.iconClass = "fa fa-plus";
            return config;
        },

        prepareAction: function(actionContext, config, callback) {

            actionContext.currentPath = actionContext.observable("path").get();
            actionContext.docId = actionContext.observable("_doc").get();
            callback();
        },

        executeAction: function(actionContext, config, callback) {

            var self = this;

            console.log("Inside executeAction");
            console.log("Current path: " + actionContext.currentPath);
            console.log("Doc ID: " + actionContext.docId);

        },

        createHandler: function(actionContext, props, callback) {
        }

    }));
});
